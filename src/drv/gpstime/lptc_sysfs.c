//lptc specific sysfs definitions

static ssize_t lptc_sysfs_status_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_lptc_status(&cdsPciModules));
}

static ssize_t lptc_sysfs_bpstatus_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_bp_status(&cdsPciModules));
}

static ssize_t lptc_sysfs_bpconfig_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_bp_config(&cdsPciModules));
}

//diagnostics files
static ssize_t lptc_sysfs_board_id_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_board_id(&cdsPciModules));
}

static ssize_t lptc_sysfs_board_sn_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_board_sn(&cdsPciModules));
}

static ssize_t lptc_sysfs_software_id_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_software_id(&cdsPciModules));
}

static ssize_t lptc_sysfs_software_rev_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_software_rev(&cdsPciModules));
}

static ssize_t lptc_sysfs_mod_address_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_mod_address(&cdsPciModules));
}

static ssize_t lptc_sysfs_gps_sec_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "%u", lptc_get_gps_sec_diag(&cdsPciModules));
}

static ssize_t lptc_sysfs_board_status_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_board_status(&cdsPciModules));
}

static ssize_t lptc_sysfs_board_config_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_board_config(&cdsPciModules));
}

static ssize_t lptc_sysfs_ocxo_controls_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_ocxo_controls(&cdsPciModules));
}

static ssize_t lptc_sysfs_ocxo_error_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_ocxo_error(&cdsPciModules));
}

static ssize_t lptc_sysfs_uplink_1pps_delay_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_uplink_1pps_delay(&cdsPciModules));
}

static ssize_t lptc_sysfs_external_1pps_delay_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_external_1pps_delay(&cdsPciModules));
}

static ssize_t lptc_sysfs_gps_1pps_delay_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_gps_1pps_delay(&cdsPciModules));
}

static ssize_t lptc_sysfs_fanout_up_loss_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_fanout_up_loss(&cdsPciModules));
}

static ssize_t lptc_sysfs_fanout_missing_delay_error_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_fanout_missing_delay_error(&cdsPciModules));
}

static ssize_t lptc_sysfs_leaps_and_error_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_leaps_and_error(&cdsPciModules));
}

// 'On Board Features' (LIGO-T2000406 3.9)

static ssize_t lptc_sysfs_brd_synch_factors_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_brd_synch_factors(&cdsPciModules));
}

static ssize_t lptc_sysfs_xadc_config_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_xadc_config(&cdsPciModules));
}

static ssize_t lptc_sysfs_board_and_powersupply_status_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_board_and_powersupply_status(&cdsPciModules));
}

static ssize_t lptc_sysfs_xadc_status_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_xadc_status(&cdsPciModules));
}

static ssize_t lptc_sysfs_temp_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    return sysfs_emit(buf, "0x%x", lptc_get_temp(&cdsPciModules));
}

static ssize_t lptc_sysfs_internal_pwr_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    int i, numc;
    u32 ips[2];
    lptc_get_internal_pwr(&cdsPciModules, ips);
    numc = sysfs_emit(buf, "0x%x,0x%x", ips[0], ips[1]);
    return numc;
}

#define NUM_EPS_REGS sizeof(((LPTC_OBF_REGISTER *)0)->eps) / sizeof(((LPTC_OBF_REGISTER *)0)->eps[0])

static ssize_t lptc_sysfs_external_pwr_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    int i, numc;
    u32 eps[NUM_EPS_REGS];
    lptc_get_external_pwr(&cdsPciModules, eps);
    numc = sysfs_emit(buf, "0x%x", eps[0]);
    for(i=1; i < NUM_EPS_REGS; i++)
    {
        numc += sysfs_emit_at(buf, numc, ",0x%x", eps[i]);
    }
    return numc;
}


// slot specific files

static ssize_t lptc_sysfs_slot_config_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    long slot = -1;
    int err;
    // +5 to account for length of "slot_" in name
    err = kstrtol(kobj->name + 5, 10, &slot);
    if(!err && slot > 0 && slot <= LPTC_BP_SLOTS)
    {
        return sysfs_emit(
            buf, "0x%x", lptc_get_slot_config( &cdsPciModules, slot - 1 ) );
    }
    else
    {
        return sysfs_emit(buf, "bad slot number");
    }
}

static ssize_t lptc_sysfs_slot_status_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    long slot = -1;
    int err;
    // +5 to account for length of "slot_" in name
    err = kstrtol(kobj->name + 5, 10, &slot);
    if(!err && slot > 0 && slot <= LPTC_BP_SLOTS)
    {
        return sysfs_emit(
            buf, "0x%x", lptc_get_slot_status( &cdsPciModules, slot - 1 ) );
    }
    else
    {
        return sysfs_emit(buf, "bad slot number");
    }
}

static ssize_t lptc_sysfs_slot_phase_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    long slot = -1;
    int err;
    // +5 to account for length of "slot_" in name
    err = kstrtol(kobj->name + 5, 10, &slot);
    if(!err && slot > 0 && slot <= LPTC_BP_SLOTS)
    {
        return sysfs_emit(
            buf, "0x%x", lptc_get_slot_phase( &cdsPciModules, slot - 1 ) );
    }
    else
    {
        return sysfs_emit(buf, "bad slot number");
    }
}

static struct kobj_attribute lptc_status_attr = __ATTR(status, 0444, lptc_sysfs_status_show, NULL);
static struct kobj_attribute lptc_backplane_status_attr = __ATTR(backplane_status, 0444, lptc_sysfs_bpstatus_show, NULL);
static struct kobj_attribute lptc_backplane_config_attr = __ATTR(backplane_config, 0444, lptc_sysfs_bpstatus_show, NULL);
static struct kobj_attribute lptc_board_id_attr = __ATTR(board_id, 0444, lptc_sysfs_board_id_show, NULL);
static struct kobj_attribute lptc_board_sn_attr = __ATTR(board_sn, 0444, lptc_sysfs_board_sn_show, NULL);
static struct kobj_attribute lptc_software_id_attr = __ATTR(software_id, 0444, lptc_sysfs_software_id_show, NULL);
static struct kobj_attribute lptc_software_rev_attr = __ATTR(software_rev, 0444, lptc_sysfs_software_rev_show, NULL);
static struct kobj_attribute lptc_gps_sec_attr = __ATTR(gps_sec, 0444, lptc_sysfs_gps_sec_show, NULL);
static struct kobj_attribute lptc_mod_address_attr = __ATTR(mod_address, 0444, lptc_sysfs_mod_address_show, NULL);
static struct kobj_attribute lptc_board_status_attr = __ATTR(board_status, 0444, lptc_sysfs_board_status_show, NULL);
static struct kobj_attribute lptc_board_config_attr = __ATTR(board_config, 0444, lptc_sysfs_board_config_show, NULL);
static struct kobj_attribute lptc_ocxo_controls_attr = __ATTR(ocxo_controls, 0444, lptc_sysfs_ocxo_controls_show, NULL);
static struct kobj_attribute lptc_ocxo_error_attr = __ATTR(ocxo_error, 0444, lptc_sysfs_ocxo_error_show, NULL);
static struct kobj_attribute lptc_uplink_1pps_delay_attr = __ATTR(uplink_1pps_delay, 0444, lptc_sysfs_uplink_1pps_delay_show, NULL);
static struct kobj_attribute lptc_external_1pps_delay_attr = __ATTR(external_1pps_delay, 0444, lptc_sysfs_external_1pps_delay_show, NULL);
static struct kobj_attribute lptc_gps_1pps_delay_attr = __ATTR(gps_1pps_delay, 0444, lptc_sysfs_gps_1pps_delay_show, NULL);
static struct kobj_attribute lptc_fanout_up_loss_attr = __ATTR(fanout_up_loss, 0444, lptc_sysfs_fanout_up_loss_show, NULL);
static struct kobj_attribute lptc_fanout_missing_delay_error_attr = __ATTR(fanout_missing_delay_error, 0444, lptc_sysfs_fanout_missing_delay_error_show, NULL);
static struct kobj_attribute lptc_leaps_and_error_attr = __ATTR(leaps_and_status, 0444, lptc_sysfs_leaps_and_error_show, NULL);
static struct kobj_attribute lptc_brd_synch_factors_attr = __ATTR(brd_synch_factors, 0444, lptc_sysfs_brd_synch_factors_show, NULL);
static struct kobj_attribute lptc_xadc_config_attr = __ATTR(xadc_config, 0444, lptc_sysfs_xadc_config_show, NULL);
static struct kobj_attribute lptc_board_and_powersupply_status_attr = __ATTR(board_and_powersupply_status, 0444, lptc_sysfs_board_and_powersupply_status_show, NULL);
static struct kobj_attribute lptc_xadc_status_attr = __ATTR(xadc_status, 0444, lptc_sysfs_xadc_status_show, NULL);
static struct kobj_attribute lptc_temp_attr = __ATTR(temp, 0444, lptc_sysfs_temp_show, NULL);
static struct kobj_attribute lptc_internal_pwr_attr = __ATTR(internal_pwr, 0444, lptc_sysfs_internal_pwr_show, NULL);
static struct kobj_attribute lptc_external_pwr_attr = __ATTR(external_pwr, 0444, lptc_sysfs_external_pwr_show, NULL);

static struct attribute *lptc_fields[] = {
    &lptc_status_attr.attr,
    &lptc_backplane_status_attr.attr,
    &lptc_backplane_config_attr.attr,
    &lptc_board_id_attr.attr,
    &lptc_board_sn_attr.attr,
    &lptc_software_id_attr.attr,
    &lptc_software_rev_attr.attr,
    &lptc_gps_sec_attr.attr,
    &lptc_mod_address_attr.attr,
    &lptc_board_status_attr.attr,
    &lptc_board_config_attr.attr,
    &lptc_ocxo_controls_attr.attr,
    &lptc_ocxo_error_attr.attr,
    &lptc_uplink_1pps_delay_attr.attr,
    &lptc_external_1pps_delay_attr.attr,
    &lptc_gps_1pps_delay_attr.attr,
    &lptc_fanout_up_loss_attr.attr,
    &lptc_fanout_missing_delay_error_attr.attr,
    &lptc_leaps_and_error_attr.attr,
    &lptc_brd_synch_factors_attr.attr,
    &lptc_xadc_config_attr.attr,
    &lptc_board_and_powersupply_status_attr.attr,
    &lptc_xadc_status_attr.attr,
    &lptc_temp_attr.attr,
    &lptc_internal_pwr_attr.attr,
    &lptc_external_pwr_attr.attr,
    NULL,
};

static struct attribute_group lptc_attr_group = {
    .attrs = lptc_fields,
};


static struct kobj_attribute lptc_slot_config_attr = __ATTR(config, 0444, lptc_sysfs_slot_config_show, NULL);
static struct kobj_attribute lptc_slot_status_attr = __ATTR(status, 0444, lptc_sysfs_slot_status_show, NULL);
static struct kobj_attribute lptc_slot_phase_attr = __ATTR(phase, 0444, lptc_sysfs_slot_phase_show, NULL);

static struct attribute *lptc_slot_fields[] = {
    &lptc_slot_config_attr.attr,
    &lptc_slot_status_attr.attr,
    &lptc_slot_phase_attr.attr,
    NULL,
};

static struct attribute_group lptc_slot_attr_group = {
    .attrs = lptc_slot_fields,
};

// returns 0 on success
static int
add_lptc_sys_files(struct kobject *parent)
{
    struct kobject *lptc_sysfs_dir = kobject_create_and_add("lptc", parent);
    int slot;

    if (NULL == lptc_sysfs_dir)
    {
        printk(KERN_ERR "could not create /sys/kernel/gpstime/lptc\n");
        return 1;
    }
    if( sysfs_create_group(lptc_sysfs_dir, &lptc_attr_group) )
    {
        printk("Could not create /sys/kernel/gpstime/lptc...fields");
        return 1;
    }

    for(slot=0; slot < LPTC_BP_SLOTS; slot++)
    {
        char buf[16];
        struct kobject *slot_sysfs_dir;
        sprintf(buf, "slot_%d", slot + 1);
        slot_sysfs_dir = kobject_create_and_add(buf, lptc_sysfs_dir);
        if (NULL == slot_sysfs_dir)
        {
            printk(KERN_ERR "could not create /sys/kernel/gpstime/lptc/%d\n", slot + 1);
            return 1;
        }
        if( sysfs_create_group(slot_sysfs_dir, &lptc_slot_attr_group))
        {
            printk("Could not create /sys/kernel/gpstime/lptc/%d...fields", slot + 1);
            return 1;
        }
    }

    return 0;
}