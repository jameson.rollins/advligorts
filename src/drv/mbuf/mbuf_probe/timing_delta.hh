//
// Created by jonathan.hanks on 5/19/21.
//

#ifndef DAQD_TRUNK_TIMING_DELTA_HH
#define DAQD_TRUNK_TIMING_DELTA_HH

#include "mbuf_probe.hh"

namespace timing_delta
{
    int calculate_timing_delta( volatile void*    buffer1,
                                volatile void*    buffer2,
                                const ConfigOpts& opts );
}

#endif // DAQD_TRUNK_TIMING_DELTA_HH
