//
// Created by jonathan.hanks on 10/20/17.
//

#ifndef TRANSFER_GPS_H
#define TRANSFER_GPS_H

#include <ostream>

#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <unistd.h>

#include <drv/gpsclock.h>

/**
 * Operations on GPS time.
 * Note for purposes of this testing, this does not return GPS time but system time.
 */
namespace GPS {

    struct gps_time
    {
        long sec;
        long nanosec;

        gps_time(): sec(0), nanosec(0) {}
        explicit gps_time(long s): sec(s), nanosec(0) {}
        gps_time(long s, long ns): sec(s), nanosec(ns) {}
        gps_time(const gps_time& other): sec(other.sec), nanosec(other.nanosec) {}

        gps_time operator-(const gps_time& other) const
        {

            gps_time result(sec - other.sec, nanosec - other.nanosec);
            while (result.nanosec < 0) {
                result.nanosec += 1000000000;
                --result.sec;
            }
            return result;
        }

        gps_time operator+(const gps_time& other) const
        {
            gps_time result(sec + other.sec, nanosec + other.nanosec);
            while (result.nanosec >= 1000000000) {
                result.nanosec -= 1000000000;
                ++result.sec;
            }
            return result;
        }

        bool operator==(const gps_time& other) const
        {
            return (sec == other.sec && nanosec == other.nanosec);
        }

        bool operator!=(const gps_time& other) const
        {
            return !(*this == other);
        }

        bool operator<(const gps_time& other) const
        {
            if (sec < other.sec) return true;
            if (sec > other.sec) return false;
            return (nanosec < other.nanosec);
        }
    };

    std::ostream& operator<<(std::ostream& os, const gps_time& gps)
    {
        os << gps.sec << ":" << gps.nanosec;
        return os;
    }


    class gps_clock
    {
    private:
        int fd_;
        int offset_;
        bool ok_;

        // these variable are used to track the lagging nanosecond glitch
        // when present, reported nanoseconds are always from the previous read.
        bool lagging_ns_glitch_ = false;  // when true, the glitch has been detected
        long last_second_ = -1;  // the last second read.  Used to check for the
                                    // lagging nanosecond glitch.
        int seconds_checked_ = 0; // how many seconds we've checked
        const int MAX_SECONDS_CHECKED_ = 10;  // limit on how many times we check for glitch
        const long GLITCH_CUTOFF_US_ = 750000;  // cutoff in microseconds.  Any time jump this big or bigger on second boundary is a glitch.

        static const int SYMMETRICOM_STATUS = 0;
        static const int SYMMETRICOM_TIME = 1;
        static bool symm_ok(int fd)
        {
            if (fd < 0) return false;
            unsigned long req = 0;
            ioctl (fd, gps_clock::SYMMETRICOM_STATUS, &req);
            return req != 0;
        }

    public:
        explicit gps_clock(int offset):fd_(open ("/dev/gpstime", O_RDWR | O_SYNC)),
                                       offset_(offset),
                                       ok_(gps_clock::symm_ok(fd_))
        {
            if (!ok_)
            {
                gpsclock_init();
            }
        }
        ~gps_clock()
        {
            if (fd_ >= 0) close(fd_);
        }
        bool ok() const
        {
            return ok_;
        }

        gps_time now()
        {
            gps_time result;

            if (ok_)
            {

                //read twice, because nanoseconds always lags by one reading for some reason
                unsigned long t[3];
                ioctl (fd_, gps_clock::SYMMETRICOM_TIME, &t);
                if(lagging_ns_glitch_)
                {

                    //read again to get nanoseconds
                    unsigned long t2[ 3 ];
                    ioctl( fd_, gps_clock::SYMMETRICOM_TIME, &t2 );
                    t[1] = t2[1];
                    t[2] = t2[2];
                }
                else
                {
                    //check for glitch
                    if(MAX_SECONDS_CHECKED_ > seconds_checked_)
                    {
                        if(last_second_ >= 0 && last_second_ < t[0])
                        {
                            last_second_ = t[0];
                            ++seconds_checked_;
                            if(t[1] > GLITCH_CUTOFF_US_)
                            {
                                lagging_ns_glitch_ = true;
                            }
                        }
                        else
                        {
                            last_second_ = t[0];
                        }
                    }

                }

                result.sec = t[0] + offset_;

                t[1] *= 1000;
                t[1] += t[2];
                result.nanosec = t[1];

            }
            else
            {
                unsigned long req[3];
                gpsclock_time(req);
                result.sec = req[0] + offset_;
                result.nanosec = req[1]*1000 + req[2];
            }
            return result;
        }
    };
}

#endif //TRANSFER_GPS_H
