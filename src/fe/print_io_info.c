/// @file print_io_info.c
/// @brief File contains routine to print IO info on code startup.

void print_io_info( CDS_HARDWARE*, int );

void
print_io_info( CDS_HARDWARE* cdsp, int iopmodel )
{
    int ii, jj, kk;
    int channels = 0;
    jj = 0;

#ifndef USER_SPACE
    printf( "" SYSTEM_NAME_STRING_LOWER ":startup time is %ld\n",
            current_time_fe( ) );
    printf( "" SYSTEM_NAME_STRING_LOWER ":cpu clock %u\n", cpu_khz );
#endif
    printf( "" SYSTEM_NAME_STRING_LOWER ":EPICSM at 0x%lx\n",
            (unsigned long)_epics_shm );
    printf( "" SYSTEM_NAME_STRING_LOWER ":TPSM at 0x%lx\n",
            (unsigned long)_tp_shm );
    printf( "" SYSTEM_NAME_STRING_LOWER ":AWGSM at 0x%lx\n",
            (unsigned long)_awg_shm );
    printf( "" SYSTEM_NAME_STRING_LOWER ":IPC    at 0x%lx\n",
            (unsigned long)_ipc_shm );
    printf( "" SYSTEM_NAME_STRING_LOWER ":IOMEM  at 0x%lx size 0x%lx\n",
            ( (unsigned long)_ipc_shm + 0x4000 ),
            sizeof( IO_MEM_DATA ) );
    printf( "" SYSTEM_NAME_STRING_LOWER ":DAQSM at 0x%lx\n",
            (unsigned long)_daq_shm );
    printf( "" SYSTEM_NAME_STRING_LOWER ":configured to use %d cards\n",
            cdsp->cards );
    kk = 0;
    printf( "******************************************************************"
            "*********\n" );
    printf( "" SYSTEM_NAME_STRING_LOWER ":%d ADC cards found\n",
            cdsp->adcCount );
    for ( ii = 0; ii < cdsp->adcCount; ii++ )
    {
        kk++;
        printf( "" SYSTEM_NAME_STRING_LOWER ":\tADC %d is a %s module\n",
                ii,
                _cdscardtypename[ cdsp->adcType[ ii ] ] );
        if ( iopmodel )
        {
            printf( "" SYSTEM_NAME_STRING_LOWER ":\t\tChannels = %d \n",
                    cdsp->adcChannels[ ii ] );
            printf( "" SYSTEM_NAME_STRING_LOWER ":\t\tFirmware Rev = %d \n\n",
                    ( cdsp->adcConfig[ ii ] & 0xfff ) );
        }
        else
        {
            printf( "" SYSTEM_NAME_STRING_LOWER ":\tMemory at block %d\n",
                    cdsp->adcConfig[ ii ] );
        }
    }
    printf( "******************************************************************"
            "*********\n" );
    printf( "" SYSTEM_NAME_STRING_LOWER ":%d DAC cards found\n",
            cdsp->dacCount );
    for ( ii = 0; ii < cdsp->dacCount; ii++ )
    {
        kk++;
        printf( "" SYSTEM_NAME_STRING_LOWER ":\tDAC %d is a %s module\n",
                ii,
                _cdscardtypename[ cdsp->dacType[ ii ] ] );
        printf( "" SYSTEM_NAME_STRING_LOWER ":\tCard number is %d\n",
                cdsp->dacInstance[ ii ] );
        printf( "" SYSTEM_NAME_STRING_LOWER ":\tMemory at block %d\n",
                cdsp->dacConfig[ ii ] );
        if ( iopmodel )
        {
            if ( cdsp->dacType[ ii ] == GSC_16AO16 )
                channels = 16;
            else
                channels = 8;
            printf( "" SYSTEM_NAME_STRING_LOWER ":\tChannels = %d \n",
                    channels );
            printf( "" SYSTEM_NAME_STRING_LOWER ":\tFirmware Rev = %d \n\n",
                    ( cdsp->dacAcr[ ii ] & 0xfff ) );
        }
    }
    kk += cdsp->doCount;
    printf( "******************************************************************"
            "*********\n" );
    printf( "" SYSTEM_NAME_STRING_LOWER ":%d DIO cards found\n",
            cdsp->dioCount );
    printf( "******************************************************************"
            "*********\n" );
    printf( "" SYSTEM_NAME_STRING_LOWER ":%d IIRO-8 Isolated DIO cards found\n",
            cdsp->card_count[ ACS_8DIO ] );
    printf( "******************************************************************"
            "*********\n" );
    printf( "" SYSTEM_NAME_STRING_LOWER
            ":%d IIRO-16 Isolated DIO cards found\n",
            cdsp->card_count[ ACS_16DIO ] );
    printf( "******************************************************************"
            "*********\n" );
    printf( "" SYSTEM_NAME_STRING_LOWER ":%d Contec 32ch PCIe DO cards found\n",
            cdsp->card_count[ CON_32DO ] );
    printf( "" SYSTEM_NAME_STRING_LOWER ":%d Contec PCIe DIO1616 cards found\n",
            cdsp->card_count[ CON_1616DIO ] );
    pLocalEpics->epicsOutput.bioMon[ 2 ] = cdsp->card_count[ CON_1616DIO ];
    printf( "" SYSTEM_NAME_STRING_LOWER ":%d Contec PCIe DIO6464 cards found\n",
            cdsp->card_count[ CON_6464DIO ] );
    pLocalEpics->epicsOutput.bioMon[ 3 ] = cdsp->card_count[ CON_6464DIO ];
    printf( "" SYSTEM_NAME_STRING_LOWER ":%d Contec PCIe CDO64 cards found\n",
            cdsp->card_count[ CDO64 ] );
    pLocalEpics->epicsOutput.bioMon[ 1 ] = cdsp->card_count[ CDO64 ];
    printf( "" SYSTEM_NAME_STRING_LOWER ":%d Contec PCIe CDI64 cards found\n",
            cdsp->card_count[ CDI64 ] );
    pLocalEpics->epicsOutput.bioMon[ 0 ] = cdsp->card_count[ CDI64 ];
    printf( "" SYSTEM_NAME_STRING_LOWER ":%d DO cards found\n", cdsp->doCount );
    printf( "" SYSTEM_NAME_STRING_LOWER
            ":Total of %d I/O modules found and mapped\n",
            kk );
    printf( "******************************************************************"
            "*********\n" );
    printf( "" SYSTEM_NAME_STRING_LOWER ":%d RFM cards found\n",
            cdsp->rfmCount );
    for ( ii = 0; ii < cdsp->rfmCount; ii++ )
    {
        printf( "\tRFM %d is a VMIC_%x module with Node ID %d\n",
                ii,
                cdsp->rfmType[ ii ],
                cdsp->rfmConfig[ ii ] );
        printf( "address is 0x%lx\n", cdsp->pci_rfm[ ii ] );
    }
    printf( "******************************************************************"
            "*********\n" );
    if ( cdsp->gps )
    {
        printf( "" SYSTEM_NAME_STRING_LOWER ":IRIG-B card found %d\n",
                cdsp->gpsType );
        printf( "**************************************************************"
                "*************\n" );
    }
    for ( ii = 0; ii < cdsp->dolphinCount; ii++ )
    {
        printf( "\tDolphin found %d\n", ii );
        printf( "Read address is 0x%lx\n", cdsp->dolphinRead[ ii ] );
        printf( "Write address is 0x%lx\n", cdsp->dolphinWrite[ ii ] );
    }
}
