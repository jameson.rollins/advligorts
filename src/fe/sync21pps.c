// Code to synch FE startup with 1PPS signal on last channel
// of first ADC module.
// Clocks are already running, so can't enable all ADC cards
// at once as some may trigger on one clock and one or more
// on the next clock, leaving them out of sync with each other.
// So idea is to enable one ADC to find the clock and then
// enable all cards in time frame before next clock.

int
sync2pps_signal( one_pps_sync_t* p1pps, adcInfo_t* padcinfo, int cpuClock[] )
{
    int           status;
    int           onePps;
    int           pps_wait = 0;
    int           sync = SYNC_SRC_1PPS;
    unsigned long nanotime = 100000;
    int ii;
    int jj = 0;
    int tc = 1;

    
    // Have to enable all of the DAC cards here.
    // Takes too long to run this after ADC startup.
    // This precludes DAC FIFO preload.
    gscEnableDacModule( &cdsPciModules, GSAI_ALL_CARDS, DAC_CLK_ENABLE );

    // Following will startup 1 ADC at a time.
    for(ii=0;ii<(cdsPciModules.adcCount - 1);ii++)
    {
    for(jj=0;jj<tc;jj++)
        gscEnableAdcModule( &cdsPciModules, jj, GSAI_DMA_MODE );
    // Wait for DMA done to indicate an ADC clock just occurred
    for(jj=0;jj<tc;jj++)
        status = plx9056_wait_dma_done( jj, 1, 100 );
    tc ++;
    }
    // Found a clock so now enable all cards
    gscEnableAdcModule( &cdsPciModules, GSAI_ALL_CARDS, GSAI_DMA_MODE );
    // Now search for the 1PPS sync pulse
    // on last channel of 1st ADC
    do
    {
        status = iop_adc_read( padcinfo, cpuClock );
        p1pps->value = dWord[ ADC_ONEPPS_BRD ][ ADC_DUOTONE_CHAN ][ 0 ];
        pps_wait++;
    } while ( p1pps->value < ONE_PPS_THRESH && pps_wait < p1pps->time );
#ifdef TEST_NOSYNC
    pps_wait = p1pps->time + 1;
#endif
    // If 1PPS signal not found, use internal clock
    if ( pps_wait >= p1pps->time )
    {
        // Sync21PPS failed, so default to no sync
        sync = SYNC_SRC_NONE;
        pps_wait = 0;
        // Now search for the start of 1sec from internal clock
        do
        {
            status = iop_adc_read( padcinfo, cpuClock );
            nanotime = current_nanosecond( );
            pps_wait++;
        } while ( nanotime < 50000 && pps_wait < p1pps->time );
    }
    cycleNum = 0;
    return sync;
}

int
sync21pps_check( one_pps_sync_t* p1pps, adcInfo_t* padcinfo )
{
    // Assign chan 32 to onePps
    p1pps->value = adcinfo.adcData[ ADC_ONEPPS_BRD ][ ADC_DUOTONE_CHAN ];
    if ( ( p1pps->value > ONE_PPS_THRESH ) && ( p1pps->signalHi == 0 ) )
    {
        p1pps->time = cycleNum;
        p1pps->signalHi = 1;
    }
    if ( p1pps->value < ONE_PPS_THRESH )
        p1pps->signalHi = 0;
}
