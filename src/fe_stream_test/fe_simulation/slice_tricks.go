package fe_simulation

import "C"
import (
	"reflect"
	"unsafe"
)

// #include <stdint.h>
import "C"

func int16Slice(data uintptr, count int) ([]C.int16_t, uintptr) {
	header := reflect.SliceHeader{
		Data: data,
		Len:  count,
		Cap:  count,
	}
	return *(*[]C.int16_t)(unsafe.Pointer(&header)), data + (uintptr(2) * uintptr(count))
}

func int32Slice(data uintptr, count int) ([]C.int32_t, uintptr) {
	header := reflect.SliceHeader{
		Data: data,
		Len:  count,
		Cap:  count,
	}
	return *(*[]C.int32_t)(unsafe.Pointer(&header)), data + (uintptr(4) * uintptr(count))
}

func int64Slice(data uintptr, count int) ([]C.int64_t, uintptr) {
	header := reflect.SliceHeader{
		Data: data,
		Len:  count,
		Cap:  count,
	}
	return *(*[]C.int64_t)(unsafe.Pointer(&header)), data + (uintptr(8) * uintptr(count))
}

func float32Slice(data uintptr, count int) ([]C.float, uintptr) {
	header := reflect.SliceHeader{
		Data: data,
		Len:  count,
		Cap:  count,
	}
	return *(*[]C.float)(unsafe.Pointer(&header)), data + (uintptr(4) * uintptr(count))
}

func float64Slice(data uintptr, count int) ([]C.double, uintptr) {
	header := reflect.SliceHeader{
		Data: data,
		Len:  count,
		Cap:  count,
	}
	return *(*[]C.double)(unsafe.Pointer(&header)), data + (uintptr(8) * uintptr(count))
}

func uint32Slice(data uintptr, count int) ([]C.uint32_t, uintptr) {
	header := reflect.SliceHeader{
		Data: data,
		Len:  count,
		Cap:  count,
	}
	return *(*[]C.uint32_t)(unsafe.Pointer(&header)), data + (uintptr(4) * uintptr(count))
}
