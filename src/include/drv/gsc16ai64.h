///	\file gsc16ai64.h
///	\brief GSC 16bit, 32 channel ADC Module Definitions. See
///<	<a
///<href="http://www.generalstandards.com/view-products2.php?BD_family=16ai64ssc">GSC
///<16AI64SSC Manual</a>
///< for more info on board registers.

#define ADC_SS_ID                                                              \
    0x3101 ///< Subsystem ID to identify and locate module on PCI bus

// Function Prototypes
int gsc16ai64Init( CDS_HARDWARE*, struct pci_dev* );
void gsc16ai64AdcStop( int );
#ifdef DIAG_TEST
void gsc16ai64DmaBump( int, int );
#endif

#define GSAI_FULL_DIFFERENTIAL 0x200
#define GSAI_64_CHANNEL 0x6
#define GSAI_32_CHANNEL 0x5
#define GSAI_8_CHANNEL 0x3
#define GSAI_SOFT_TRIGGER 0x1000
#define GSAI_RESET 0x8000
#define GSAI_CHAN_COUNT 32
#ifdef SERVO512K
#define GSAI_DMA_BYTE_COUNT 0x80
#define GSAI_64_OFFSET GSAI_CHAN_COUNT - 1
#else
#define GSAI_DMA_BYTE_COUNT ( 0x80 * UNDERSAMPLE )
#define GSAI_64_OFFSET ( GSAI_CHAN_COUNT * UNDERSAMPLE - 1 )
#endif
#define GSAI_SAMPLE_START 0x10000
#define GSAI_EXTERNAL_SYNC 0x10
#define GSAI_AUTO_CAL 0x2000
#define GSAI_AUTO_CAL_PASS 0x4000
#define GSAI_DATA_CODE_OFFSET 0x8000
#define GSAI_DATA_MASK 0xffff
#define GSC16AI64_OSC_FREQ 50000000
