
/* PLX Chip Definitions for GSA ADC/DAC Modules ******************************* */
/* Common DMA register definition       */
typedef struct PLX_9056_DMA{
        u32 pad[32];   /* DMA register is at 0x80 offset from base of PLX chip */
        u32 DMA0_MODE;         /* 0x80 */
        u32 DMA0_PCI_ADD;      /* 0x84 */
        u32 DMA0_LOC_ADD;      /* 0x88 */
        u32 DMA0_BTC;          /* 0x8C */
        u32 DMA0_DESC;         /* 0x90 */
        u32 DMA1_MODE;         /* 0x94 */
        u32 DMA1_PCI_ADD;      /* 0x98 */
        u32 DMA1_LOC_ADD;      /* 0x9C */
        u32 DMA1_BTC;          /* 0xA0 */
        u32 DMA1_DESC;         /* 0xA4 */
        u32 DMA_CSR;           /* 0xA8 */
}PLX_9056_DMA;

/* Struct to point to interrupt control register when using interrupts from ADC */
typedef struct PLX_9056_INTCTRL{
        u32 pad[26];
        u32 INTCSR;
}PLX_9056_INTCTRL;

#define PLX_VID                 0x10b5      /* PLX9056 Vendor Id    */
#define PLX_TID                 0x9056      /* PLX9056 Type Id  */

#define PLX_DMA_DONE            0x10
#define PLX_DMA_START           0x3
#define PLX_DMA1_START          0x300
#define PLX_DMA_MODE_NO_INTR    0x10943
#define PLX_DMA_LOCAL_ADDR      0x8
#define PLX_DMA_TO_PCI          0xA     // Card xfers its data to PCI ie to computer
#define PLX_DMA_FROM_PCI        0x0     // Card xdfers data from PCI ie from computer
#define PLX_DEMAND_DMA          0x1000

// DAC board local DMA addresses
// This is address of card output buffer
#define GSAO16_OUTBUF_LOCAL_ADDRESS       0x18
#define GSAO1820_OUTBUF_LOCAL_ADDRESS     0x48
// DAC board DMA byte transfer counts
#define GSAO16_BTC        64      // num chans (16) * 4 bytes
#define GSAO1820_BTC      32      // num chans (8) * 4 bytes

#ifndef USER_SPACE
volatile PLX_9056_DMA* adcDma[ MAX_ADC_MODULES ]; ///< DMA struct for GSA ADC
dma_addr_t adc_dma_handle[ MAX_ADC_MODULES ]; ///< PCI add of ADC DMA memory

volatile PLX_9056_DMA *dacDma[MAX_DAC_MODULES]; /* DMA struct for GSA DAC */
dma_addr_t dac_dma_handle[MAX_DAC_MODULES];     /* PCI add of DAC DMA memory */
#endif


