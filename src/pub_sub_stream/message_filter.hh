//
// Created by jonathan.hanks on 1/13/21.
//

#ifndef DAQD_TRUNK_MESSAGE_FILTER_HH
#define DAQD_TRUNK_MESSAGE_FILTER_HH

#include <atomic>
#include <chrono>
#include <fstream>
#include <thread>

#include <cds-pubsub/sub.hh>

class MessageFilter
{
public:
    virtual bool operator( )( const pub_sub::SubMessage& ) const = 0;
    virtual ~MessageFilter( ) = default;
};

/*!
 * @brief filter out messages that do not have self-consistent time values.
 */
class ConsistentTimeFilter : public MessageFilter
{
public:
    ~ConsistentTimeFilter( ) override = default;
    /*!
     * @brief the filter function
     * @param msg the message to check the internal times for
     * @return false if there are inconsistencies, else true
     */
    bool
    operator( )( const pub_sub::SubMessage& msg ) const override
    {
        static const std::int64_t nano_scale = 1000000000 / 16;
        if ( msg.size( ) <= sizeof( daq_multi_dcu_header_t ) )
        {
            return false;
        }
        auto headers =
            reinterpret_cast< daq_multi_dcu_header_t* >( msg.data( ) );
        if ( headers->dcuTotalModels == 0 )
        {
            return false;
        }
        auto curCycle = headers->dcuheader[ 0 ].cycle;
        auto curNano = headers->dcuheader[ 0 ].timeNSec;
        auto expectedNano = curCycle * nano_scale;

        return ( curNano == curCycle || curNano == expectedNano );
    }
};

/*!
 * @brief A simple filter to reject messages that have keys (times) wildly
 * different than current gps time.
 */
class GpsMessageFilter : public MessageFilter
{
public:
    /*!
     * @brief initialize the message filter
     * @param max_skew the max skew +-seconds allowed
     */
    explicit GpsMessageFilter( int max_skew )
        : max_skew_{ static_cast< int64_t >( max_skew ) }
    {
    }
    GpsMessageFilter( const GpsMessageFilter& ) = delete;
    GpsMessageFilter( GpsMessageFilter&& ) = delete;
    GpsMessageFilter& operator=( const GpsMessageFilter& ) = delete;
    GpsMessageFilter& operator=( GpsMessageFilter&& ) = delete;

    ~GpsMessageFilter( ) override = default;

    /*!
     * @brief Tell the run_updater routine it can exit.
     */
    void
    stop( ) noexcept
    {
        stopping_ = true;
    }

    /*!
     * @brief A main loop that can be used by a thread to keep the current gps
     * time updated.
     */
    void
    run_updater( )
    {
        while ( !stopping_ )
        {
            update_gps( );
            std::this_thread::sleep_for( std::chrono::seconds( 25 ) );
        }
    }

    /*!
     * @brief update the internal gps time value.
     * @note This attempts to read /proc/gps and if that fails falls back to
     * system time + an offset
     */
    void
    update_gps( )
    {
        std::int64_t cur_time{ 0 };
        std::fstream gps_file( "/proc/gps" );
        if ( gps_file )
        {
            gps_file >> cur_time;
            if ( gps_file.good( ) )
            {
                cur_gps_ = cur_time;
                return;
            }
        }
        auto seconds_since_epoch =
            std::chrono::duration_cast< std::chrono::seconds >(
                std::chrono::system_clock::now( ).time_since_epoch( ) );
        // yes this is hard coded, our typical window is big enough that
        // not being up to date on leap seconds is not a big issue.
        cur_time = static_cast< std::int64_t >( seconds_since_epoch.count( ) ) -
            315964782;
        cur_gps_ = cur_time;
        return;
    }

    /*!
     * @brief Get the gps time as understood by the filter
     * @return the gps time in seconds
     */
    std::int64_t
    gps( ) const noexcept
    {
        return cur_gps_;
    }

    /*!
     * @brief the filter function
     * @param msg the message to check the time (key) for
     * @return false if it is too far away from gps time else true
     */
    bool
    operator( )( const pub_sub::SubMessage& msg ) const override
    {
        auto gps = cur_gps_.load( );
        if ( gps == 0 )
        {
            return true;
        }
        auto msg_time = extract_gps( msg );
        if ( msg_time < 0 )
        {
            return false;
        }
        auto delta = msg_time - gps;
        delta = ( delta < 0 ? -delta : delta );
        return ( delta <= max_skew_ );
    }

private:
    /*!
     * @brief extract the gps time from the msg
     * @return The gps time or -1 if it could not be extracted
     */
    static std::int64_t
    extract_gps( const pub_sub::SubMessage& msg ) noexcept
    {
        if ( msg.size( ) <= sizeof( daq_multi_dcu_header_t ) )
        {
            return -1;
        }
        auto headers =
            reinterpret_cast< daq_multi_dcu_header_t* >( msg.data( ) );
        if ( headers->dcuTotalModels == 0 )
        {
            return -1;
        }
        return static_cast< std::int64_t >( headers->dcuheader[ 0 ].timeSec );
    }

    std::int64_t                max_skew_{ 0 };
    std::atomic< std::int64_t > cur_gps_{ 0 };
    std::atomic< bool >         stopping_{ false };
};

#endif // DAQD_TRUNK_MESSAGE_FILTER_HH
