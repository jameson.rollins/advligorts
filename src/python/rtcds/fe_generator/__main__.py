#!/usr/bin/python3

# Generate systemd unit files needed for LIGO CDS front-end
# server startup sequences

from rtcds.fe_generator.sequencer import Sequencer
from rtcds.fe_generator.options import get_options
import sys
from rtcds.fe_generator.log import klog


if __name__ == '__main__':
    klog(f"in python with args {sys.argv}")
    seq = Sequencer(get_options(), sys.argv[2])
    klog(f"Sequencer created")
    seq.create_start_sequence()
    klog(f"sequence complete")
