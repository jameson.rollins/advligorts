#!/usr/bin/python3
# find the most worthy commit for testing.
#
# 1. Most recent tag that looks like a clean version number
# 2. Most recent tag that looks like a version number
# 3. master branch
# 4. branches not merged to master, starting with latest updated, excluding packaging branches

import re

from collections import namedtuple

NamedCommit = namedtuple("NamedCommit","names commit")

bad_tags = ["4.2.7_dev2"]

def find_recent_dirty_tag(repo):
    """
    Find the most recent tag that looks like version number but is not clean.

    :param repo: a git.Repo pointing at an advligorts repository
    :return: the tag or None if nothing is found
    """
    verclean_re = re.compile(r"^\d+\.\d+\.\d+$")
    ver_re = re.compile(r"^\d+\.\d+\.\d+\w*$")

    clean_tags = [t for t in repo.tags if verclean_re.search(t.name)]

    tags = [t for t in repo.tags if ver_re.search(t.name) and t not in clean_tags and t.name not in bad_tags]
    tags = sorted(tags, reverse=True, key=lambda t: t.commit.committed_date)
    if len(tags) > 0:
        return tags[0]
    else:
        return None

def find_recent_clean_tag(repo):
    """
    Find the most recent tag that looks like version number and has nothing extra in it.

    :param repo: a git.Repo pointing at an advligorts repository
    :return: the tag or None if nothing is found
    """
    ver_re = re.compile(r"^\d+\.\d+\.\d+$")

    tags = [t for t in repo.tags if ver_re.search(t.name) if t.name not in bad_tags]
    tags = sorted(tags, reverse=True, key=lambda t: t.commit.committed_date)
    if len(tags) > 0:
        return tags[0]
    else:
        return None

def is_ancestor_of(parent, child):
    """
    Find out of parent is an ancestor commit of child
    specifically, returns true if parent and child are the same.
    :param parent: a git.Commit
    :param child: a git.Commit
    :return: True if parent is an ancestor, or the same commit as child
    """
    visited = set()
    stack = [child]
    while len(stack) > 0:
        commit = stack.pop()
        visited.add(commit)
        if commit == parent:
            return True
        for p in commit.parents:
            if p not in visited:
                stack.append(p)

    return False

def find_non_ancestors(commits, child):
    """
    Modify a set of commits to include only non-ancestors of child.
    :param commits: a set of git.Commit
    :param child: a git Commit
    :return: None
    """
    visited = set()
    stack = [child]
    while len(stack) > 0:
        commit = stack.pop()
        visited.add(commit)
        commits.discard(commit)
        for p in commit.parents:
            if p not in visited:
                stack.append(p)


def branch_commit_name(branch):
    """
    return a string useful for naming specific points on a branch
    """
    return f"{branch.name}_{branch.commit.committed_datetime.isoformat()}"


def find_deserving_branches(repo):
    """
    Find most deserving branches for testing.
    :param repo: a git.Repo object
    :return: a list of NamedCommits, named after branch + commit time.  Sorted by commit time.
    master and package branches omitted.  Ancestors of master ommitted.
    """

    master = repo.heads.master

    skip_res = [
        re.compile(r"^master$"),
        re.compile(r"^debian/"),
        re.compile(r"^HEAD$")
        ]

    branches = []


    # get remote branches
    for r in repo.remotes:
        for ref in r.refs:
            base_name = ref.name.split("/",1)[1]
            skip = False
            for reg in skip_res:
                if reg.search(base_name):
                    skip = True
                    break
            if not skip:
                branches.append(ref)

    commits = set([b.commit for b in branches])
    find_non_ancestors(commits, master.commit)
    find_non_ancestors(commits,repo.remotes[0].refs["debian/buster"].commit)
    branches = [b for b in branches if b.commit in commits]
    branches = sorted(branches, reverse=True, key=lambda b: b.commit.committed_date)

    return [NamedCommit([b.name, branch_commit_name(b)], b.commit) for b in branches]


def get_deserving_commits(repo):
    """
    Get a list of NamedCommits in order of priority we want to try.

    :param repo: a git.Repo object
    :return: a list of NamedCommits.  First item is highest priority.
    """

    commits = []

    # latest clean version
    tag = find_recent_clean_tag(repo)
    if tag is not None:
        commits.append( NamedCommit([tag.name], tag.commit))

    # latest unclean version
    tag = find_recent_dirty_tag(repo)
    if tag is not None:
        commits.append( NamedCommit([tag.name], tag.commit))

    # master branch
    master = repo.heads.master
    if master is not None:
        name = branch_commit_name(master)
        commits.append(NamedCommit(["master",name], master.commit))

    # other branches
    commits += find_deserving_branches(repo)

    return commits

def test(repo):
    commits = get_deserving_commits(repo)
    print("\n".join([str(c) for c in commits]))

    print(repo.remotes[0].refs["branch-2.1"].name)


if __name__ == "__main__":
    from git import Repo

    repo = Repo("/tmp/advligorts")
    #repo = Repo.clone_from('https://git.ligo.org/cds/advligorts.git', "/tmp/advligorts")
    #repo = Repo("/home/erik.vonreis/projects/advligorts")
    test(repo)