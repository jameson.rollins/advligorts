/*!	\mainpage RCG Automated Regression Testing 
\tableofcontents

\section s1 RCG Regression Test Overview 
\verbatim
Real-time Code Generator (RCG) regression testing is developed as a number of Perl and Python scripts.
These scripts are located in the CDS advligorts GIT repositiory under the advligorts/test/rcgtest 
directory. The tests are specifically designed to run on a stand-alone system, presently x2ats at LLO.
\endverbatim
\subsection ss1 Hardware
\verbatim
Use of the RCG test scripts requires the following hardware:
- Stand-alone CDS FE computer, specifically x2ats, located in DTS0 at LLO.
- I/O chassis
  - Tests require the latest I/O chassis with PCIe timing card and associated IOC backplane.
- Various ADC/DAC and Contec binary I/O modules
  - The IOP model (x2iopsam) is presently configured to use:
    1) Three (3) GSC16AI64SSC ADC modules.
    2) One GSC18AI64SSC ADC module.
    3) One GSC16AO16 DAC module.
    4) One GSC20AO8 DAC module.
    5) Three (3) GSC18AO8 DAC modules
    6) Two (2) Contec6464 Binary I/O modules.
- DAC to ADC loopback chassis, one 16bit DAC and one 18bit DAC
\endverbatim
\subsection ss2 Control Models
\verbatim
The RCG auto test requires 5 control models to operate:
  - x2iopsam (IOP model )
  - x2atstim32 ( Control App Model (CAM) running at 32K )
  - x2atstim16 ( CAM running at 16K )
  - x2atstim04 ( CAM running at 4K )
  - x2atstim02 ( CAM running at 2K )
These models can be found in the advligorts/test/rcgtest/models directory.
\endverbatim
\subsection ss3 MEDM Screens
\verbatim
A number of custom MEDM screens are provided for test support in the advligorts/test/rcgtest/medm 
directory. The primary screens are:
\endverbatim
<b>- AUTOTEST_OVERVIEW.adl</b>
\verbatim
A test overview screen is provided to show the test system running models and DAQ status. This screen
is divided into 4 primary sections:
1) State words and DAQ status of the running control models.  This section also has related display
   links to the model GDSTP screens and to the test control screen.
2) DAQ status.
3) Control model timing information.
4) I/O module status. This section also contains reset buttons for the watchdogs built into the IOP
   and 32K control model. These must be reset allow DAC outputs to provide the loopback timing in
   the timing information section.
\endverbatim

\image html autotestover.png "RCG TEST OVERVIEW SCREEN"
\image latex autotestover.png "RCG TEST OVERVIEW SCREEN"
<b>- AUTOTEST_CNTL.adl</b>
\verbatim
This display provides test status information and allows for the manualstartup of test scripts. 
Test system status is shown at the top center, "TEST SYSTEM - IDLE" on the example scrren shot. 
The remainder of the screen provides:
1) Grouping of 4 general types of tests
   a) IOP testing, include I/O
   b) RCG Filter module tests
   c) Matrix tests
   d) Miscellaneous RCG part testing
2) A gray action button that will start a test when selected
3) Individual test status blocks to the right of the start test button
   a) Red/green to immediate right indicating pass/fail of last time test was run.
   b) Indicator to right of test status, which goes yellow when the test is in progress.
4) Blue related display to the left of the start button if there is an MEDM screen related to the test.
5) A cream color action button above each set of the 4 test groups. Selecting this icon will run all 
   of the tests within that group, one at a time.
\endverbatim
\image html testcontrol.png "RCG TEST CONTROL SCREEN"
\image latex testcontrol.png "RCG TEST CONTROL SCREEN"
\section sec2 Initiating RCG Regression Tests 
<b>Tests are designed to run one at a time, as some tests make use of common test model part settings or \n
DTT resources.</b> \n
\verbatim
The regression tests can be started either by using the test control screen or from the command line, 
using the rcgtest.pl command in the advligorts/test/rcgtest/scripts directory. Arguments to the 
rcgtest command start a group of tests or individual tests. Command arguments are:
\endverbatim

<b>testIO:</b> Sequentially runs the following 4 tests:\n
   <b>ioptest:</b> Tests various functions of the IOP.\n
   <b>duotone:</b> Varifies IOP time synchronization by using the ADC duotone signal.\n
   <b>binaryIO:</b> Tests operation of Contec6464 binary I/O cards.\n
   <b>dac:</b> Tests for proper operation and control model mapping of DAC cards.\n\n

<b>testfilters:</b> Sequentially runs the following 6 filter module related tests:\n
   <b>daqfilter:</b> \n
   <b>decfilter:</b> \n
   <b>fmc:</b> \n
   <b>fmc2:</b> \n
   <b>sfm:</b> \n
   <b>inputfilter:</b> \n\n

<b>matrixtest:</b> Sequentially runs the following matrix tests:\n
   <b>matrix:</b> \n
   <b>rampmuxmatrix:</b> \n\n

<b>miscparts:</b> Sequentially runs the following tests:\n
   <b>dackilltimed:</b> \n
   <b>osc:</b> \n
   <b>phase:</b> \n
   <b>exctp:</b> \n
   <b>epics_in_ctl:</b> \n
   <b>simparts:</b> \n

\section ss5 Generating RCG Test Documentation 
\verbatim
The test software provides a method to generate the test document (this document) using doxygen and 
latex. The script that generates this document is advligorts/test/rcgtest/doc/makedoc.sh. The resulting
pdf file will be put in the doc directory as rcgtest.pdf. 
Alternatively, the document can be produced by depressing the 'Create Test Report' button on the
RCG test control MEDM screen.
\endverbatim
\section ss4 Software 
\verbatim
The software used to perform the regression tests is written as Perl and Python scripts, found in the
advligorts/test/rcgtest/perl and python directories. Tests are designed to run one at a time, as some 
tests make use of common test model part settings or DTT resources. 
\endverbatim
\section sec4 Summary test results
    \ref testsummary
 


*/

