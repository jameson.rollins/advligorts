#!/usr/bin/perl -w 
##
##

$clock = $ARGV[0] * 1000000;
$freqreq = $ARGV[1];
$freqlow = $freqreq - 1000;
$freqhi = $freqreq + 1000;

# $clock = 36000000;
$newdiv = 0.0;
$newdivint = 0;
$dclk = 0.0;
$freq = 0.0;
$bestset = 100000;
for($ii=1;$ii<50;$ii++) {
    $dclk = $clock / $ii;
  for($jj=1;$jj<1000;$jj++) {
    $bclk = $dclk / $jj;
    $newdiv = $bclk / $freqreq;
    $newdivint = int($newdiv);
    $newdivplus = $newdiv + 1;
    $freq = $clock / $ii / $jj;
    if($freq < $freqhi and $freq > $freqlow) {
    print "New clock div = $ii \t $jj\t$freq \n";
    }
    $newset = abs($freq - $freqreq);
    if($newset < $bestset) {
        $bestset = $newset;
        $bestseta = $ii;
        $bestsetb = $jj;
        $bestfreq = $freq;
    }
  }
}
$finalfreq = $clock / ($bestseta * $bestsetb);
print "Best setting is $bestseta with $bestsetb at $bestfreq\t$finalfreq\n";
